package nl.entreco.dsc.domain.entities;

import java.util.List;
import java.util.Map;

import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 27-1-2016.
 */
public class ScoreTracker {
    private int mNumberOfTeams;

    private List<Integer> mTeamSizes;

    private List<Turn> mValidatedTurns;

    private Map<Team, Score> mTeamScores;

    public ScoreTracker(){

    }
}
