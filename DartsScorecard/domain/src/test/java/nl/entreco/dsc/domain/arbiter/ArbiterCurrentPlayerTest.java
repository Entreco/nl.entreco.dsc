package nl.entreco.dsc.domain.arbiter;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import nl.entreco.dsc.domain.entities.Arbiter;
import nl.entreco.dsc.domain.entities.BaseRules;
import nl.entreco.dsc.domain.entities.Turn;
import nl.entreco.dsc.domain.player.HumanPlayer;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.team.HumanTeam;
import nl.entreco.dsc.domain.team.Team;

import static org.junit.Assert.assertEquals;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public class ArbiterCurrentPlayerTest {

    private final BaseRules.Builder mRulesBuilder = new BaseRules.Builder();

    Player player1 = new HumanPlayer("1", "Remco1", "1", "");

    Player player2 = new HumanPlayer("2", "Remco2", "2", "");

    Player player3 = new HumanPlayer("3", "Remco3", "3", "");

    Player player4 = new HumanPlayer("4", "Remco4", "4", "");

    Player player5 = new HumanPlayer("5", "Remco5", "5", "");

    Player player6 = new HumanPlayer("6", "Remco6", "6", "");

    Player player7 = new HumanPlayer("7", "Remco7", "7", "");

    Player player8 = new HumanPlayer("8", "Remco8", "8", "");

    @Test
    public void testOneManTeam() {
        Arbiter arbiter = new Arbiter.Builder(mRulesBuilder.withTeams(ts(t("1", player1))).build())
                .build();
        assertOrderOfThrowing(arbiter, player1);
    }

    @Test
    public void testOneVsOne() {
        Arbiter arbiter = new Arbiter.Builder(mRulesBuilder.withTeams(ts(
                t("1", player1),
                t("2", player2))).build()).build();
        assertOrderOfThrowing(arbiter, player1, player2);
    }

    @Test
    public void testTwoVsTwo() {
        Arbiter arbiter = new Arbiter.Builder(mRulesBuilder.withTeams(ts(
                t("1", player1, player2),
                t("2", player3, player4))).build()).build();
        assertOrderOfThrowing(arbiter, player1, player3, player2, player4);
    }

    @Test
    public void tesOneVsThreeVsTwo() {
        Arbiter arbiter = new Arbiter.Builder(mRulesBuilder.withTeams(ts(
                t("1", player1),
                t("2", player2, player3, player4),
                t("3", player5, player6))).build()).build();
        assertOrderOfThrowing(arbiter,
                player1, player2, player5,
                player1, player3, player6,
                player1, player4, player5,
                player1, player2, player6,
                player1, player3, player5,
                player1, player4, player6);
    }

    @Test
    public void tesTwoVsTwoVsTwoVsTwo() {
        Arbiter arbiter = new Arbiter.Builder(mRulesBuilder.withTeams(ts(
                t("1", player1, player2),
                t("2", player3, player4),
                t("3", player5, player6),
                t("4", player7, player8))).build()).build();
        assertOrderOfThrowing(arbiter,
                player1, player3, player5, player7,
                player2, player4, player6, player8);
    }

    private void assertOrderOfThrowing(final Arbiter _arbiter, final Player... _players) {
        for (int i = 0; i < 100; i++) {
            expect(_players[i % _players.length], _arbiter);
        }
    }

    private void expect(final Player _player, final Arbiter _arbiter) {
        assertEquals(_player, _arbiter.getCurrentPlayer());
        _arbiter.validateScore(_player, new Turn(1, 1, 1, 1, 1, 1));
    }

    private List<Team> ts(Team... _teams) {
        return Arrays.asList(_teams);
    }

    private Team t(String _uuid, Player... players) {
        return new HumanTeam(_uuid, Arrays.asList(players));
    }


}
