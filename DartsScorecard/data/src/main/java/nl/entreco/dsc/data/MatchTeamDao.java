package nl.entreco.dsc.data;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import android.support.annotation.NonNull;

import java.util.UUID;

import nl.entreco.dsc.data.match.MatchDao;
import nl.entreco.dsc.data.team.TeamDao;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
@Table(name = "MatchTeams")
public class MatchTeamDao extends Model {

    public static class Columns {

        public static final String UUID = "uuid";

        public static final String MATCH = "match";

        public static final String TEAM = "team";
    }


    @Column(name = Columns.UUID)
    public final String mUuid;

    @Column(name = Columns.TEAM)
    public final String mTeam;

    @Column(name = Columns.MATCH)
    public final String mMatch;

    public MatchTeamDao() {
        this(UUID.randomUUID().toString(), UUID.randomUUID().toString());
    }

    public MatchTeamDao(@NonNull final String _matchUuid, @NonNull final String _teamUuid) {
        mUuid = UUID.randomUUID().toString();
        mMatch = _matchUuid;
        mTeam = _teamUuid;
    }

    @NonNull
    public TeamDao getTeam() {
        return new Select().from(TeamDao.class).where(TeamDao.Columns.UUID + "=?", mTeam)
                .executeSingle();
    }

    @NonNull
    public MatchDao getMatch() {
        return new Select().from(MatchDao.class).where(MatchDao.Columns.UUID + "=?", mMatch)
                .executeSingle();
    }
}
