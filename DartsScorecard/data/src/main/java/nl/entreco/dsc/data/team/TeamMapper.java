package nl.entreco.dsc.data.team;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.dsc.data.players.PlayerMapper;
import nl.entreco.dsc.domain.team.ComputerTeam;
import nl.entreco.dsc.domain.team.HumanTeam;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class TeamMapper {

    @NonNull
    static Team daoToDomain(@NonNull final TeamDao _team, final boolean isHuman) {
        return isHuman
                ? new HumanTeam(_team.getUuid(), PlayerMapper.daoToDomain(_team.getPlayers(),
                isHuman))
                : new ComputerTeam(_team.getUuid(),
                        PlayerMapper.daoToDomain(_team.getPlayers(), isHuman));
    }

    @NonNull
    public static List<Team> daoToDomain(@NonNull final List<TeamDao> _teams) {
        List<Team> teams = new ArrayList<>(_teams.size());
        for (TeamDao teamDao : _teams) {
            teams.add(daoToDomain(teamDao, true));
        }
        return teams;
    }
}
