package nl.entreco.dsc.data.match;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import nl.entreco.dsc.data.MatchTeamDao;
import nl.entreco.dsc.data.team.TeamDao;
import nl.entreco.dsc.domain.match.BaseMatch;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
@Table(name = "Matches")
public class MatchDao extends Model {

    public static class Columns {

        public static final String UUID = "uuid";

        public static final String TYPE = "type";

        public static final String STARTED = "started";
    }

    @Column(name = Columns.UUID)
    final String mUuid;

    @Column(name = Columns.TYPE)
    final int mType;

    @Column(name = Columns.STARTED)
    final long mStartTime;

    public MatchDao() {
        mUuid = UUID.randomUUID().toString();
        mType = BaseMatch.TYPE_DEFAULT;
        mStartTime = System.currentTimeMillis();
    }

    public String getUuid() {
        return mUuid;
    }

    public int getType() {
        return mType;
    }

    public List<TeamDao> getTeams() {
        return new Select()
                .from(TeamDao.class)
                .innerJoin(MatchTeamDao.class).on(
                        "Teams.uuid" + " = " + "MatchTeams.team")
                .where(MatchTeamDao.Columns.MATCH + "=?", mUuid)
                .execute();
    }
}
