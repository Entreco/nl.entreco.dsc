package nl.entreco.dsc.domain.entities;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public interface Score {

    int score();

    int sets();

    int legs();

    Score newLeg(int _startScore);

    Score newSet(int _startScore);

    class Builder {

        private static final int NORMAL = 0;

        private static final int NEW_LEG = 1;

        private static final int NEW_SET = 2;

        private static final int GAME_SHOT_MATCH = 3;

        private final Rules mRules;

        private int mScore;

        private int mSets;

        private int mLegs;

        private int mType;


        public Builder(final Rules _rules, final Score _score) {
            this.mRules = _rules;
            this.mScore = _score.score();
            this.mSets = _score.sets();
            this.mLegs = _score.legs();
            this.mType = NORMAL;
        }

        public Score build() {
            switch (this.mType) {
                case NEW_LEG:
                    return new Score01.Leg(mScore, mSets, mLegs);
                case NEW_SET:
                    return new Score01.Set(mScore, mSets, mLegs);
                case GAME_SHOT_MATCH:
                    return new Score01.Match(mScore, mSets, mLegs);
                default:
                    return new Score01(mScore, mSets, mLegs);

            }
        }

        public Builder withTurn(final Turn _scored) {
            this.mScore -= _scored.getTotal();
            return this;
        }

        public Builder forFinish() {
            this.mScore = 0;
            if (mRules.requiresNewLeg(this.mLegs)) {
                this.mType = NEW_LEG;
                this.mLegs++;
            } else if (mRules.requiresNewSet(this.mSets)) {
                this.mType = NEW_SET;
                this.mLegs = 0;
                this.mSets++;
            } else {
                // GAME, SHOT & MATCH
                this.mType = GAME_SHOT_MATCH;
            }
            return this;
        }
    }
}
