package nl.entreco.dsc.data;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import nl.entreco.dsc.data.players.PlayerDaoProvider;
import nl.entreco.dsc.data.team.TeamDaoProvider;
import nl.entreco.dsc.domain.PlayerManager;
import nl.entreco.dsc.domain.TeamManager;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;
import nl.entreco.dsc.domain.team.Team;
import nl.entreco.dsc.domain.team.TeamUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class TeamTest extends DscApplicationTestcase {

    private PlayerManager mPlayerManager;

    private TeamManager mTeamManager;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mPlayerManager = new PlayerManager(new PlayerDaoProvider());
        mTeamManager = new TeamManager(new TeamDaoProvider());
    }

    @Test
    public void createHumanTeam() throws UnknownPlayerException {
        final Player player1 = mPlayerManager.createHumanPlayer("Remco", "20", "");
        final Player player2 = mPlayerManager.createHumanPlayer("Eva", "none", "");
        Team team1 = mTeamManager.create(player1, player2);
        assertNotNull(team1);
        assertSame(2, team1.getPlayers().size());
        assertEquals(TeamUtils.teamNameForPlayers(Arrays.asList(player1, player2)),
                team1.getTeamName());
        assertTrue(team1.isHuman());
        assertEquals("Remco", team1.getPlayers().get(0).getName());
        assertEquals("Eva", team1.getPlayers().get(1).getName());
    }

    @Test
    public void createComputerTeam() throws UnknownPlayerException {
        final Player computer1 = mPlayerManager.createComputerPlayer();
        final Player computer2 = mPlayerManager.createComputerPlayer();
        Team team = mTeamManager.create(computer1, computer2);
        assertNotNull(team);
        assertFalse(team.isHuman());
        assertEquals(TeamUtils.teamNameForPlayers(Arrays.asList(computer1, computer2)),
                team.getTeamName());
        assertEquals(computer1, team.getPlayer(0));
        assertEquals(computer2, team.getPlayer(1));
        assertEquals("Computer 1", team.getPlayers().get(0).getName());
        assertEquals("Computer 2", team.getPlayers().get(1).getName());
    }
}
