package nl.entreco.dsc.domain;

import android.support.annotation.NonNull;

import java.util.List;

import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public interface TeamProvider {

    Team create(@NonNull final Player... _players) throws UnknownPlayerException;

    List<Team> retrieveTeams(List<String> _teamUuids);
}
