package nl.entreco.dsc.data;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import android.support.annotation.NonNull;

import java.util.UUID;

import nl.entreco.dsc.data.players.PlayerDao;
import nl.entreco.dsc.data.team.TeamDao;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
@Table(name = "TeamPlayers")
public class TeamPlayerDao extends Model {

    public static class Columns {

        public static final String UUID = "uuid";

        public static final String TEAM = "team";

        public static final String PLAYER = "player";
    }

    @Column(name = Columns.UUID)
    public final String mUuid;

    @Column(name = Columns.TEAM)
    public final String mTeam;

    @Column(name = Columns.PLAYER)
    public final String mPlayer;

    public TeamPlayerDao() {
        this(UUID.randomUUID().toString(), UUID.randomUUID().toString());
    }

    public TeamPlayerDao(@NonNull final String _teamUuid, @NonNull final String _playerUuid) {
        mUuid = UUID.randomUUID().toString();
        mTeam = _teamUuid;
        mPlayer = _playerUuid;
    }

    @NonNull
    public TeamDao getTeam() {
        return new Select().from(TeamDao.class).where(TeamDao.Columns.UUID + "=?", mTeam)
                .executeSingle();
    }

    @NonNull
    public PlayerDao getPlayer() {
        return new Select().from(PlayerDao.class).where(PlayerDao.Columns.UUID + "=?", mPlayer)
                .executeSingle();
    }
}
