package nl.entreco.dsc.domain;

import android.support.annotation.NonNull;

import java.util.List;

import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class TeamManager implements TeamProvider {

    private final TeamProvider mTeamProvider;

    public TeamManager(TeamProvider _provider) {
        mTeamProvider = _provider;
    }

    @Override
    public Team create(@NonNull final Player... _players) throws UnknownPlayerException {
        return mTeamProvider.create(_players);
    }

    @Override
    public List<Team> retrieveTeams(final List<String> _teamUuids) {
        return mTeamProvider.retrieveTeams(_teamUuids);
    }
}
