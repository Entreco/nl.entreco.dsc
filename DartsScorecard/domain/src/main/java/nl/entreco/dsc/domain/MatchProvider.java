package nl.entreco.dsc.domain;

import java.util.List;

import nl.entreco.dsc.domain.match.Match;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
public interface MatchProvider {

    Match retrieveMatch(final String _uuid);

    Match createMatch01(List<Team> _teams);
}
