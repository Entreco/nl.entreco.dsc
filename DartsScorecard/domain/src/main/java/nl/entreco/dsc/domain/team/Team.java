package nl.entreco.dsc.domain.team;

import java.util.List;

import nl.entreco.dsc.domain.player.Player;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public interface Team {

    String getUuid();

    List<Player> getPlayers();

    String getTeamName();

    boolean isHuman();

    Player getPlayer(int _index);
}
