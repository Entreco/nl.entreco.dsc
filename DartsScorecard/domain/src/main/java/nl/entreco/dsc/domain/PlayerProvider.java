package nl.entreco.dsc.domain;

import android.support.annotation.NonNull;

import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public interface PlayerProvider {

    @NonNull
    Player createHumanPlayer(
            @NonNull final String _name,
            @NonNull final String _double,
            @NonNull final String _profile);

    @NonNull
    Player retrieveHumanPlayer(@NonNull final String _uuid) throws UnknownPlayerException;

    @NonNull
    Player createComputerPlayer();
}
