package nl.entreco.dsc.domain.entities;

import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public interface Arbiter {

    Player validateScore(final Player _player, final Turn _turn);

    DartThrowable undoScore();

    Player getCurrentPlayer();

    Team getCurrentTeam();

    class Builder {

        final Rules mRules;

        public Builder(final Rules _rules) {
            this.mRules = _rules;
        }

        public Arbiter build() {
            return new BaseArbiter(this);
        }
    }
}
