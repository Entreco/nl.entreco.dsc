package nl.entreco.dsc.domain.player;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public class HumanPlayer extends BasePlayer {

    public HumanPlayer(final String _uuid, final String _name, final String _favDouble,
            final String _profile) {
        super(_uuid, _name, _favDouble, _profile);
    }

    @Override
    public boolean isHuman() {
        return true;
    }
}
