package nl.entreco.dsc.domain.player;

import android.support.annotation.NonNull;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public abstract class BasePlayer implements Player {

    private final String mUuid;

    private final String mName;

    private final String mFavDouble;

    private final String mProfilePic;

    public BasePlayer(final String _uuid, final String _name, final String _favDouble,
            final String _profile) {
        mUuid = _uuid;
        mName = _name;
        mFavDouble = _favDouble;
        mProfilePic = _profile;
    }

    @NonNull
    @Override
    public String getUuid() {
        return mUuid;
    }

    @NonNull
    @Override
    public String getName() {
        return mName;
    }

    @NonNull
    @Override
    public String getFavDouble() {
        return mFavDouble;
    }

    @NonNull
    @Override
    public String getProfilePic() {
        return mProfilePic;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final BasePlayer that = (BasePlayer) o;

        return getUuid().equals(that.getUuid());

    }

    @Override
    public int hashCode() {
        return getUuid().hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", getName(), getFavDouble());
    }
}
