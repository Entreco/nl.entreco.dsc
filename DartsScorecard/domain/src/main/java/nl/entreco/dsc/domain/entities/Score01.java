package nl.entreco.dsc.domain.entities;

/**
 * Created by Remco Janssen on 27-1-2016.
 */
public class Score01 implements Score {

    private final int mScore;

    private final int mSet;

    private final int mLeg;

    public Score01(final int _score, final int _set, final int _leg) {
        this.mScore = _score;
        this.mSet = _set;
        this.mLeg = _leg;
    }

    @Override
    public int score() {
        return mScore;
    }

    @Override
    public int sets() {
        return mSet;
    }

    @Override
    public int legs() {
        return mLeg;
    }

    @Override
    public Score01 newLeg(int _startScore) {
        return new Score01(_startScore,
                this.mSet,
                this.mLeg);
    }

    @Override
    public Score01 newSet(int _startScore) {
        return new Score01(_startScore,
                this.mSet,
                this.mLeg);
    }

    @Override
    public String toString() {
        return "" + sets() + "|" + legs() + "|" + score();
    }

    static class Leg extends Score01 {

        public Leg(final int _score, final int _set, final int _leg) {
            super(_score, _set, _leg);
        }

    }

    static class Set extends Score01 {

        public Set(final int _score, final int _set, final int _leg) {
            super(_score, _set, _leg);
        }
    }

    static class Match extends Score01 {

        public Match(final int _score, final int _set, final int _leg) {
            super(_score, _set, _leg);
        }
    }
}
