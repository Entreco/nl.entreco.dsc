package nl.entreco.dsc.domain.match;

import java.util.List;

import nl.entreco.dsc.domain.entities.Arbiter;
import nl.entreco.dsc.domain.entities.Rules;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
public class Match01 extends BaseMatch {

    public Match01(final String _uuid, final List<Team> _teams) {
        super(_uuid, _teams);
    }

    @Override
    protected final int getType() {
        return TYPE_01;
    }

    @Override
    protected Arbiter prepareArbiterWithRules(final Rules _rules) {
        return new Arbiter.Builder(_rules).build();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onFinish() {

    }
}
