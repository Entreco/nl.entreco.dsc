package nl.entreco.dsc.domain.entities;

/**
 * Created by Remco Janssen on 27-1-2016.
 */
public class ValidatedTurn implements DartThrowable {

    private final Turn mTurn;

    public ValidatedTurn(final Turn _turn) {
        this.mTurn = _turn;
    }

    @Override
    public int getTotal() {
        return mTurn.getTotal();
    }
}
