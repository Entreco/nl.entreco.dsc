package nl.entreco.dsc.domain.entities;

import nl.entreco.dsc.domain.exceptions.InvalidTurnException;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 27-1-2016.
 */
public interface Rules {

    Player getCurrentPlayer();

    Team getCurrentTeam();

    Score initialScore();

    Score validate(final Turn _turn) throws InvalidTurnException;

    void storeEverything(final Score _score, final ValidatedTurn _turn);

    boolean requiresNewLeg(int _legs);

    boolean requiresNewSet(int _sets);

    ValidatedTurn undo();
}
