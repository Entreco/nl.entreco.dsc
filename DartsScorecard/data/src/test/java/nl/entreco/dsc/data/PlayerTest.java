package nl.entreco.dsc.data;

import org.junit.Before;
import org.junit.Test;

import nl.entreco.dsc.data.players.PlayerDaoProvider;
import nl.entreco.dsc.domain.PlayerManager;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public class PlayerTest extends DscApplicationTestcase {

    private PlayerManager mPlayerManager;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mPlayerManager = new PlayerManager(new PlayerDaoProvider());
    }

    @Test
    public void testCreatePlayer() {
        Player remco = mPlayerManager.createHumanPlayer("Remco", "20", "file:///content/media/18");
        assertNotNull(remco);
        assertEquals("Remco", remco.getName());
        assertEquals("20", remco.getFavDouble());
        assertEquals("file:///content/media/18", remco.getProfilePic());
        assertNotNull(remco.getUuid());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePlayerWithInvalidName() {
        mPlayerManager.createHumanPlayer(null, "", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePlayerWithInvalidDouble() {
        mPlayerManager.createHumanPlayer("ok", null, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePlayerWithInvalidProfile() {
        mPlayerManager.createHumanPlayer("some name", "", null);
    }

    @Test
    public void testCreatePlayerWithInvalidProfileUriDoesNotThrowAnException() {
        final String profileUri = "this is not a valid uri";
        Player player = mPlayerManager.createHumanPlayer("some name", "", profileUri);
        assertNotNull(player);
        assertEquals(profileUri, player.getProfilePic());
    }

    @Test
    public void testRetrievePlayer() throws Exception {
        Player remco = mPlayerManager.createHumanPlayer("Remco", "20", "file:///content/media/18");
        Player samePlayer = mPlayerManager.retrieveHumanPlayer(remco.getUuid());
        assertEquals(remco.getName(), samePlayer.getName());
        assertEquals(remco.getFavDouble(), samePlayer.getFavDouble());
        assertEquals(remco.getProfilePic(), samePlayer.getProfilePic());
    }

    @Test(expected = UnknownPlayerException.class)
    public void testRetrieveNonExistingPlayer() throws UnknownPlayerException {
        Player noPlayer = mPlayerManager.retrieveHumanPlayer("nonsense");
        assertNull(noPlayer);
    }
}
