package nl.entreco.dsc.data.players;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.UUID;

import nl.entreco.dsc.data.team.TeamDao;
import nl.entreco.dsc.data.TeamPlayerDao;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
@Table(name = "Players")
public class PlayerDao extends Model {

    public static class Columns {

        public static final String UUID = "uuid";

        public static final String NAME = "name";

        public static final String FAV_DOUBLE = "fav_double";

        public static final String PROFILE = "profile";
    }

    @Column(name = Columns.UUID)
    private final String mUuid;

    @Column(name = Columns.NAME)
    private String mName;

    @Column(name = Columns.FAV_DOUBLE)
    private String mFavDouble;

    @Column(name = Columns.PROFILE)
    private String mProfile;

    public PlayerDao() {
        mUuid = UUID.randomUUID().toString();
        mName = "";
        mFavDouble = "";
        mProfile = "";
    }

    @NonNull
    public String getUuid() {
        return mUuid;
    }

    @NonNull
    public String getName() {
        return mName;
    }

    public void setName(final String _name) {
        if (_name == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        mName = _name;
    }

    @NonNull
    public String getFavDouble() {
        return mFavDouble;
    }

    public void setFavDouble(final String _favDouble) {
        if (_favDouble == null) {
            throw new IllegalArgumentException("Favourite double may not be null");
        }
        mFavDouble = _favDouble;
    }

    @NonNull
    public String getProfile() {
        return mProfile;
    }

    public void setProfile(final String _profile) {
        if (_profile == null) {
            throw new IllegalArgumentException("Profile may not be null");
        }
        mProfile = _profile;
    }

    public List<TeamDao> getTeams() {
        return new Select()
                .from(TeamDao.class)
                .innerJoin(TeamPlayerDao.class).on(
                        "Teams.uuid" + " = " + "TeamPlayers.player")
                .where(TeamPlayerDao.Columns.PLAYER + "=?", mUuid)
                .execute();
    }
}
