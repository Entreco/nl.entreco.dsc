package nl.entreco.dsc.data.players;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.dsc.domain.player.ComputerPlayer;
import nl.entreco.dsc.domain.player.HumanPlayer;
import nl.entreco.dsc.domain.player.Player;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class PlayerMapper {

    @NonNull
    public static List<Player> daoToDomain(@NonNull final List<PlayerDao> _player,
            final boolean _isHuman) {
        List<Player> players = new ArrayList<>(_player.size());
        for (PlayerDao dao : _player) {
            players.add(daoToDomain(dao, _isHuman));
        }
        return players;
    }

    @NonNull
    static Player daoToDomain(@NonNull final PlayerDao _player, final boolean _isHuman) {
        return _isHuman ?
                new HumanPlayer(_player.getUuid(), _player.getName(), _player.getFavDouble(),
                        _player.getProfile()) :
                new ComputerPlayer(_player.getUuid(), _player.getName(), _player.getFavDouble(),
                        _player.getProfile());
    }
}
