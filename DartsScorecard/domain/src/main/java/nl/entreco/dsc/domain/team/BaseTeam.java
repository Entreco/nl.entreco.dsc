package nl.entreco.dsc.domain.team;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.dsc.domain.player.Player;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
abstract class BaseTeam implements Team {

    private final String mUuid;

    private final List<Player> mPlayers;

    private final String mTeamname;

    BaseTeam(final String _uuid, final List<Player> _players) {
        mUuid = _uuid;
        mPlayers = _players == null ? new ArrayList<>() : _players;
        mTeamname = TeamUtils.teamNameForPlayers(mPlayers);
    }

    @Override
    public String getUuid() {
        return mUuid;
    }

    @Override
    public List<Player> getPlayers() {
        return mPlayers;
    }

    @Override
    public String getTeamName() {
        return mTeamname;
    }

    @Override
    public Player getPlayer(final int _index) {
        return mPlayers.get(_index);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final BaseTeam baseTeam = (BaseTeam) o;

        return getUuid().equals(baseTeam.getUuid());

    }

    @Override
    public int hashCode() {
        return getUuid().hashCode();
    }
}
