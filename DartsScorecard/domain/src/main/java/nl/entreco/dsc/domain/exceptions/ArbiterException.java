package nl.entreco.dsc.domain.exceptions;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public abstract class ArbiterException extends Exception {

    public abstract String getReason();
}
