package nl.entreco.dsc.domain;

import java.util.List;

import nl.entreco.dsc.domain.match.Match;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
public class MatchManager implements MatchProvider {

    private final MatchProvider mMatchProvider;

    public MatchManager(MatchProvider _matchProvider) {
        mMatchProvider = _matchProvider;
    }

    @Override
    public Match createMatch01(final List<Team> _teams) {
        return mMatchProvider.createMatch01(_teams);
    }

    @Override
    public Match retrieveMatch(final String _uuid) {
        return mMatchProvider.retrieveMatch(_uuid);
    }
}
