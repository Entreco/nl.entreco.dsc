package nl.entreco.dsc.data.match;

import com.activeandroid.query.Select;

import java.util.List;

import nl.entreco.dsc.data.MatchTeamDao;
import nl.entreco.dsc.data.team.TeamDao;
import nl.entreco.dsc.data.team.TeamMapper;
import nl.entreco.dsc.domain.MatchProvider;
import nl.entreco.dsc.domain.match.Match;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
public class MatchDaoProvider implements MatchProvider {

    @Override
    public Match retrieveMatch(final String _uuid) {
        MatchDao matchDao = new Select().from(MatchDao.class)
                .where(MatchDao.Columns.UUID + "=?", _uuid).executeSingle();
        List<TeamDao> teams = matchDao.getTeams();
        return MatchMapper.daoToDomain(matchDao, TeamMapper.daoToDomain(teams));
    }

    @Override
    public Match createMatch01(final List<Team> _teams) {
        MatchDao matchDao = new MatchDao();
        matchDao.save();

        for (Team team : _teams) {
            MatchTeamDao matchTeamDao = new MatchTeamDao(matchDao.getUuid(), team.getUuid());
            matchTeamDao.save();
        }
        return MatchMapper.daoToDomain(matchDao, _teams);
    }
}
