package nl.entreco.dsc.domain.exceptions;

import nl.entreco.dsc.domain.player.Player;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public class NotYourTurnException extends ArbiterException {

    private final Player mPlayer;

    public NotYourTurnException(Player _actualPlayer) {
        mPlayer = _actualPlayer;
    }

    public Player whoIsSupposedToSubmitAScore() {
        return mPlayer;
    }

    @Override
    public String getReason() {
        return String.format("It's %s turn", mPlayer);
    }
}
