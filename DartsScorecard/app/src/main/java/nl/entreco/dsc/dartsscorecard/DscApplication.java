package nl.entreco.dsc.dartsscorecard;

import com.activeandroid.ActiveAndroid;

import android.app.Application;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public class DscApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDb();
    }

    private void initializeDb() {
        ActiveAndroid.initialize(this);
    }
}
