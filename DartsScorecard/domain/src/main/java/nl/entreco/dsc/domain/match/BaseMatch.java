package nl.entreco.dsc.domain.match;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.dsc.domain.entities.Arbiter;
import nl.entreco.dsc.domain.entities.Rules;
import nl.entreco.dsc.domain.entities.Turn;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
public abstract class BaseMatch implements Match {

    public static final int TYPE_01 = 1;

    public static final int TYPE_DEFAULT = TYPE_01;

    private final int mType;

    private final String mUuid;

    private final List<Team> mTeams;

    private Arbiter mArbiter;

    public BaseMatch(final String _uuid, final List<Team> _teams) {
        mUuid = _uuid;
        mTeams = _teams == null ? new ArrayList<>() : _teams;
        mType = getType();
    }

    protected abstract Arbiter prepareArbiterWithRules(final Rules _rules);

    protected abstract int getType();

    @Override
    public final void onStartWithRules(final Rules _rules) {
        mArbiter = prepareArbiterWithRules(_rules);
        System.out
                .println(String.format("Game on, %s to throw first", mArbiter.getCurrentPlayer()));
    }

    @Override
    public final String getUuid() {
        return mUuid;
    }

    @Override
    public final int getNumberOfTeams() {
        return mTeams.size();
    }

    @Override
    public final int getNumberOfPlayers() {
        int numberOfPlayers = 0;
        for (Team team : mTeams) {
            numberOfPlayers += team.getPlayers().size();
        }
        return numberOfPlayers;
    }

    @Override
    public Team getTeam(final int _index) {
        return mTeams.get(_index);
    }

    @NonNull
    @Override
    public Player doSubmitScore(@NonNull final Player _player, @NonNull final Turn _turn) {
        return mArbiter.validateScore(_player, _turn);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final BaseMatch baseMatch = (BaseMatch) o;

        if (getType() != baseMatch.getType()) {
            return false;
        }
        return getUuid().equals(baseMatch.getUuid());

    }

    @Override
    public int hashCode() {
        int result = getType();
        result = 31 * result + getUuid().hashCode();
        return result;
    }
}
