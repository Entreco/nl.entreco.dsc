package nl.entreco.dsc.domain;

import android.support.annotation.NonNull;

import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class PlayerManager implements PlayerProvider {

    private final PlayerProvider mPlayerManager;

    public PlayerManager(PlayerProvider _manager) {
        mPlayerManager = _manager;
    }

    @NonNull
    @Override
    public Player createHumanPlayer(@NonNull final String _name, @NonNull final String _double,
            @NonNull final String _profile) {
        return mPlayerManager.createHumanPlayer(_name, _double, _profile);
    }

    @NonNull
    @Override
    public Player retrieveHumanPlayer(@NonNull final String _uuid) throws UnknownPlayerException {
        return mPlayerManager.retrieveHumanPlayer(_uuid);
    }

    @NonNull
    @Override
    public Player createComputerPlayer() {
        return mPlayerManager.createComputerPlayer();
    }
}
