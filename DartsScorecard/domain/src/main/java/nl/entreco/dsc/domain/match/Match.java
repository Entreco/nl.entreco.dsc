package nl.entreco.dsc.domain.match;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import nl.entreco.dsc.domain.entities.Rules;
import nl.entreco.dsc.domain.entities.Turn;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public interface Match {

    void onStartWithRules(final Rules _rules);

    void onPause();

    void onResume();

    void onFinish();

    @NonNull
    @CheckResult
    Player doSubmitScore(final Player _player, final Turn _turn);

    String getUuid();

    int getNumberOfTeams();

    int getNumberOfPlayers();

    Team getTeam(int _index);
}
