package nl.entreco.dsc.domain.player;

import android.support.annotation.NonNull;

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public interface Player {

    @NonNull
    String getUuid();

    @NonNull
    String getName();

    @NonNull
    String getFavDouble();

    @NonNull
    String getProfilePic();

    boolean isHuman();
}
