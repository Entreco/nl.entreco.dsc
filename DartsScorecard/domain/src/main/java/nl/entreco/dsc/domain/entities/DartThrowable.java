package nl.entreco.dsc.domain.entities;

/**
 * Created by Remco Janssen on 27-1-2016.
 */
public interface DartThrowable {
    int getTotal();
}
