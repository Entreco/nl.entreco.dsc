package nl.entreco.dsc.data;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.support.annotation.CallSuper;

import nl.entreco.dsc.data.match.MatchDao;
import nl.entreco.dsc.data.players.PlayerDao;
import nl.entreco.dsc.data.team.TeamDao;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, application = ApplicationStub.class, sdk = 21)
public abstract class DscApplicationTestcase {

    @Before
    @CallSuper
    public void setUp() throws Exception {

        /**
         * NOTE: Initializing ActiveAndroid with a context
         * fails on MAC...
         *
         * <code>
         *     ActiveAndroid.initialize(this); // fails on mac
         * </code>
         *
         * hence, we initialize ActiveAndroid with
         * a Configuration
         */

        @SuppressWarnings("unchecked")
        Configuration config = new Configuration.Builder(RuntimeEnvironment.application)
                .addModelClasses(
                        PlayerDao.class,
                        TeamDao.class,
                        MatchDao.class,
                        TeamPlayerDao.class,
                        MatchTeamDao.class
                        )
                .create();
        ActiveAndroid.initialize(config);
    }

    @After
    public void tearDown() throws Exception {
        ActiveAndroid.dispose();
    }
}
