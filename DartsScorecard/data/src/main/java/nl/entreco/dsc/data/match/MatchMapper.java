package nl.entreco.dsc.data.match;

import java.util.List;

import nl.entreco.dsc.domain.match.BaseMatch;
import nl.entreco.dsc.domain.match.Match;
import nl.entreco.dsc.domain.match.Match01;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
public class MatchMapper {

    static Match daoToDomain(final MatchDao _matchDao, final List<Team> _teams) {
        switch (_matchDao.getType()) {
            case BaseMatch.TYPE_01:
                return new Match01(_matchDao.getUuid(), _teams);
            default:
                return new Match01(_matchDao.getUuid(), _teams);
        }
    }
}
