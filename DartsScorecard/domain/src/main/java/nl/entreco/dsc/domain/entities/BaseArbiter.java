package nl.entreco.dsc.domain.entities;

import android.support.annotation.Nullable;

import nl.entreco.dsc.domain.exceptions.ArbiterException;
import nl.entreco.dsc.domain.exceptions.InvalidTurnException;
import nl.entreco.dsc.domain.exceptions.NotYourTurnException;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public class BaseArbiter implements Arbiter {

    private final Rules mRules;

    public BaseArbiter(final Builder _builder) {
        System.out.println("You're MasterCaller for the evening... Mr. Fitzgerald");
        mRules = _builder.mRules;
    }

    @Override
    public final Player validateScore(final Player _player, final Turn _turn) {
        try {
            checkPlayersTurn(_player);  // check players turn
            Score validatedScore = mRules.validate(_turn);// validate score
            return acceptScore(_player, _turn, validatedScore);
        } catch (NotYourTurnException | InvalidTurnException turnNotAllowedOrInvalid) {
            return declineScore(_player, turnNotAllowedOrInvalid);
        }
    }

    private void checkPlayersTurn(final Player _player) throws NotYourTurnException {
        Player playerThatShouldSubmitScore = getCurrentPlayer();
        if (!playerThatShouldSubmitScore.equals(_player)) {
            throw new NotYourTurnException(playerThatShouldSubmitScore);
        }
    }

    protected Player acceptScore(final Player _player, final Turn _turn, final Score _score) {

        // Store all results.
        mRules.storeEverything(_score, new ValidatedTurn(_turn));

        // get Player to throw next.
        Player nextPlayer = getCurrentPlayer();

        // Print some output to Console, for debugging
        if (_score instanceof Score01.Leg) {
            System.out.println(
                    String.format("Yeahhhh, that's the x'd leg shot! %s, threw %d, score:%s",
                            _player, _turn.getTotal(),
                            _score));

            System.out.println(
                    String.format("%s to throw first",
                            nextPlayer));
        } else if (_score instanceof Score01.Set) {
            System.out.println(
                    String.format("Yeahhhh, that's the x'd set.\n%s, threw %d, score:%s", _player,
                            _turn
                                    .getTotal(), _score));
        } else if (_score instanceof Score01.Match) {
            System.out.println(
                    String.format("GAME, SHOT & MATCH!!! Congrats:%s", _player));
        } else {
            System.out.println(
                    String.format("%s, threw %d, score:%s", _player, _turn.getTotal(), _score));
        }

        return nextPlayer;
    }

    protected Player declineScore(final Player _player, final ArbiterException _arbiterException) {
        System.out.println(
                String.format("Sorry %s, %s", _player, _arbiterException.getReason()));
        return getCurrentPlayer();
    }

    @Nullable
    @Override
    public DartThrowable undoScore() {
        try {
            ValidatedTurn undo = mRules.undo();
            System.out.println(
                    String.format("My bad... please remove %d\n %s, you're up", undo.getTotal(),
                            getCurrentPlayer()));
            return undo;
        } catch (IndexOutOfBoundsException noMoreScoresToUndo) {
            System.out.println("Nothing to undo...");
            return null;
        }
    }

    @Override
    public Player getCurrentPlayer() {
        return mRules.getCurrentPlayer();
    }

    @Override
    public Team getCurrentTeam() {
        return mRules.getCurrentTeam();
    }
}
