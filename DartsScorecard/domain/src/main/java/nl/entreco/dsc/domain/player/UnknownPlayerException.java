package nl.entreco.dsc.domain.player;

import java.util.Locale;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class UnknownPlayerException extends Exception {

    public UnknownPlayerException(final String _msg) {
        super(_msg);
    }

    public UnknownPlayerException(final String _msg, final Object... _args) {
        super(String.format(Locale.getDefault(), _msg, _args));
    }
}
