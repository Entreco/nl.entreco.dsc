package nl.entreco.dsc.data.team;

import com.activeandroid.query.Select;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.dsc.data.TeamPlayerDao;
import nl.entreco.dsc.domain.TeamProvider;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class TeamDaoProvider implements TeamProvider {

    @NonNull
    @Override
    public Team create(@NonNull final Player... _players) throws UnknownPlayerException {
        TeamDao teamDao = new TeamDao();
        teamDao.save();
        boolean isHuman = false;
        for (Player player : _players) {
            TeamPlayerDao teamPlayer = new TeamPlayerDao(teamDao.getUuid(), player.getUuid());
            teamPlayer.save();
            isHuman |= player.isHuman();
        }
        // Set the name if all players where added.
        return TeamMapper.daoToDomain(teamDao, isHuman);
    }

    @Override
    public List<Team> retrieveTeams(final List<String> _teamUuids) {
        List<Team> teams = new ArrayList<>(_teamUuids.size());
        for (String teamUuid : _teamUuids) {
            teams.add(retrieveTeam(teamUuid));
        }
        return teams;
    }

    private Team retrieveTeam(final String _uuid) {
        TeamDao teamDao = new Select().from(TeamDao.class).where(TeamDao.Columns.UUID + "=?", _uuid)
                .executeSingle();
        return TeamMapper.daoToDomain(teamDao, true);
    }
}
