package nl.entreco.dsc.domain.team;

import android.support.annotation.NonNull;

import java.util.Collection;

import nl.entreco.dsc.domain.player.Player;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class TeamUtils {

    private static final int AVERAGE_PLAYER_NAME = 8;

    private static final String TEAM_SEPERATOR = " & ";

    @NonNull
    public static String teamNameForPlayers(@NonNull final Collection<Player> _players) {
        StringBuilder builder = new StringBuilder(_players.size() * AVERAGE_PLAYER_NAME);
        for (Player player : _players) {
            if (builder.length() > 0) {
                builder.append(TEAM_SEPERATOR);
            }
            builder.append(player.getName());
        }
        return builder.toString();
    }
}
