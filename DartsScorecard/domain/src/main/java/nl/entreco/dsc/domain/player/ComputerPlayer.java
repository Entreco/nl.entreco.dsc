package nl.entreco.dsc.domain.player;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class ComputerPlayer extends BasePlayer {

    public ComputerPlayer(final String _uuid, final String _name, final String _favDouble,
            final String _profile) {
        super(_uuid, _name, _favDouble, _profile);
    }

    @Override
    public boolean isHuman() {
        return false;
    }
}
