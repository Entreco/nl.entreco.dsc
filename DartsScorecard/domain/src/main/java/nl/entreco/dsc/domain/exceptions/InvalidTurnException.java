package nl.entreco.dsc.domain.exceptions;

import nl.entreco.dsc.domain.entities.Turn;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public class InvalidTurnException extends ArbiterException {

    private final Turn mTurn;

    public InvalidTurnException(Turn _turn) {
        mTurn = _turn;
    }

    @Override
    public String getReason() {
        return String.format("%d is invalid", mTurn.getTotal());
    }
}
