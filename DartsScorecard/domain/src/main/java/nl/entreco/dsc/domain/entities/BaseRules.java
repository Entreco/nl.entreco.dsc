package nl.entreco.dsc.domain.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.entreco.dsc.domain.exceptions.InvalidTurnException;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.team.Team;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public class BaseRules implements Rules {

    public static final int DEFAULT_START_SCORE = 501;

    public static final int DEFAULT_LEGS = 2;

    public static final int DEFAULT_SETS = 2;

    public static final boolean DEFAULT_TRAINING = false;

    public static final boolean DEFAULT_DOUBLE_IN = false;

    public static final boolean DEFAULT_DOUBLE_OUT = true;

    public static final int DEFAULT_START_INDEX = 0;

    private boolean mNewLeg;

//    private int mNumberOfLegsPlayed;

    private final int mStartIndex;

    private final int mStartScore;

    private final int mRequiredNumberOfLegs;

    private final int mRequiredNumberOfSets;

    private final boolean mIsTraining;

    private final boolean mDoubleIn;

    private final boolean mDoubleOut;

    private final int mNumberOfTeams;

    private final List<Team> mTeams;

    private final List<Integer> mTeamSizes;

    private final Map<Integer, List<ValidatedTurn>> mValidatedTurnsPerLeg;

    private final Map<Team, Score> mTeamScores;

    private BaseRules(final Builder _builder) {
        mStartScore = _builder.mStartScore;
        mRequiredNumberOfLegs = _builder.mNumLegs;
        mRequiredNumberOfSets = _builder.mNumSets;
        mIsTraining = _builder.mIsTraining;
        mDoubleIn = _builder.mDoubleIn;
        mDoubleOut = _builder.mDoubleOut;
        mTeams = _builder.mTeams;
        mNumberOfTeams = _builder.mNumberOfTeams;
        mTeamSizes = _builder.mTeamSizes;
        mTeamScores = _builder.mTeamScores;
        mStartIndex = _builder.mStartIndex;
        mValidatedTurnsPerLeg = new HashMap<>();
        configureNewLeg();
    }

    @Override
    public Score initialScore() {
        return new Score01(mStartScore, 0, 0);
    }

    @Override
    public Score validate(final Turn _turn) throws
            InvalidTurnException {

        final Score _current = mTeamScores.get(getCurrentTeam());
        if (_turn.getTotal() < _current.score()) {
            return new Score.Builder(this, _current).withTurn(_turn).build();
        } else if (_turn.getTotal() == _current.score()) {
            return new Score.Builder(this, _current).forFinish().build();
        }
        throw new InvalidTurnException(_turn);
    }

    @Override
    public void storeEverything(final Score _score, final ValidatedTurn _validatedTurn) {
        mNewLeg = false;
        mTeamScores.put(getCurrentTeam(), _score);
        storeTurn(_validatedTurn);

        if (_score instanceof Score01.Leg) {
            newLegPlease();
        } else if (_score instanceof Score01.Set) {
            newSetPlease();
        }
    }

    private void storeTurn(final ValidatedTurn _turn) {
        List<ValidatedTurn> turns = getTurnsInLastLeg();
        turns.add(_turn);
        mValidatedTurnsPerLeg.put(mValidatedTurnsPerLeg.size() - 1, turns);
    }

    private List<ValidatedTurn> getTurnsInLastLeg() {
        return mValidatedTurnsPerLeg.get(mValidatedTurnsPerLeg.size() - 1);
    }

    private void newSetPlease() {
        for (Map.Entry<Team, Score> entry : mTeamScores.entrySet()) {
            mTeamScores.put(entry.getKey(), entry.getValue().newSet(mStartScore));
        }
        configureNewLeg();
    }

    private void newLegPlease() {
        for (Map.Entry<Team, Score> entry : mTeamScores.entrySet()) {
            mTeamScores.put(entry.getKey(), entry.getValue().newLeg(mStartScore));
        }
        configureNewLeg();
    }

    private void configureNewLeg() {
        mNewLeg = true;
        mValidatedTurnsPerLeg.put(mValidatedTurnsPerLeg.size(), new ArrayList<>());
    }

    @Override
    public ValidatedTurn undo() {
        List<ValidatedTurn> turns = mValidatedTurnsPerLeg.get(mValidatedTurnsPerLeg.size() - 1);
        return turns.remove(turns.size() - 1);
    }

    @Override
    public boolean requiresNewLeg(final int _legs) {
        return _legs < mRequiredNumberOfLegs;
    }

    @Override
    public boolean requiresNewSet(final int _sets) {
        return _sets < mRequiredNumberOfSets;
    }

    @Override
    public Player getCurrentPlayer() {

        if (!mNewLeg) {
            int teamIndex = getNumberOfValidatedScores() % mNumberOfTeams;
            int playerIndex = (getNumberOfValidatedScores() / mNumberOfTeams) % mTeamSizes
                    .get(teamIndex);
            return getCurrentTeam().getPlayer(playerIndex);
        } else {
            int teamIndex = getNumberOfLegsPlayed() % mNumberOfTeams;
            int playerIndex = (getNumberOfLegsPlayed() / mNumberOfTeams) % mTeamSizes
                    .get(teamIndex);
            Player nextPlayer = getTeam(teamIndex).getPlayer(playerIndex);
            return nextPlayer;
        }
    }

    private Team getTeam(final int _teamIndex) {
        return mTeams.get(_teamIndex);
    }

    @Override
    public Team getCurrentTeam() {
        int teamIndex = getNumberOfValidatedScores() % mNumberOfTeams;
        return getTeam(teamIndex);
    }

    synchronized int getNumberOfValidatedScores() {
        List<ValidatedTurn> numberOfTurnsInLeg = getTurnsInLastLeg();
        return numberOfTurnsInLeg == null ? 0
                : numberOfTurnsInLeg.size() + getNumberOfLegsPlayed();
    }

    synchronized int getNumberOfLegsPlayed() {
        return mValidatedTurnsPerLeg.size() - 1 + mStartIndex;
    }

    public static class Builder {

        private int mStartScore;

        private int mNumLegs;

        private int mNumSets;

        private boolean mIsTraining;

        private boolean mDoubleIn;

        private boolean mDoubleOut;

        private List<Team> mTeams;

        private int mNumberOfTeams;

        private List<Integer> mTeamSizes;

        private Map<Team, Score> mTeamScores;

        private int mStartIndex;

        public Builder() {
            this(DEFAULT_START_SCORE, DEFAULT_LEGS, DEFAULT_SETS, DEFAULT_TRAINING,
                    DEFAULT_DOUBLE_IN, DEFAULT_DOUBLE_OUT, DEFAULT_START_INDEX);
        }

        private Builder(final int _startScore, final int _legs, final int _sets,
                final boolean _isTraining, final boolean _isDoubleIn,
                final boolean _isDoubleOut,
                final int _startIndex) {
            mStartScore = _startScore;
            mNumSets = _sets;
            mNumLegs = _legs;
            mIsTraining = _isTraining;
            mDoubleIn = _isDoubleIn;
            mDoubleOut = _isDoubleOut;
            mTeams = new ArrayList<>();
            mStartIndex = _startIndex;
        }

        public Builder withStartScore(final int _startScore) {
            mStartScore = _startScore;
            return this;
        }

        public Builder setIndexOfStartingPlayer(final int _starterIndex) {
            mStartIndex = _starterIndex;
            return this;
        }

        public Builder isTraining(final boolean _isTraining) {
            mIsTraining = _isTraining;
            return this;
        }

        public Builder useDoubleIn(final boolean _useDoubleIn) {
            mDoubleIn = _useDoubleIn;
            return this;
        }

        public Builder useDoubleOut(final boolean _useDoubleOut) {
            mDoubleOut = _useDoubleOut;
            return this;
        }

        public Builder withTeams(final List<Team> _teams) {
            this.mTeams = _teams;
            return this;
        }

        private void setupScoreTracking(Score _initialScore) {
            mNumberOfTeams = mTeams.size();
            mTeamSizes = new ArrayList<>(mTeams.size());
            mTeamScores = new HashMap<>();
            for (Team team : mTeams) {
                mTeamSizes.add(team.getPlayers().size());
                mTeamScores.put(team, _initialScore);
            }
        }

        public Rules build() {
            Rules rules = new BaseRules(this);
            setupScoreTracking(rules.initialScore());
            return new BaseRules(this);
        }
    }
}
