package nl.entreco.dsc.domain.arbiter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.entreco.dsc.domain.entities.Arbiter;
import nl.entreco.dsc.domain.entities.BaseRules;
import nl.entreco.dsc.domain.entities.Turn;
import nl.entreco.dsc.domain.exceptions.ArbiterException;
import nl.entreco.dsc.domain.player.HumanPlayer;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;
import nl.entreco.dsc.domain.team.HumanTeam;
import nl.entreco.dsc.domain.team.Team;

import static org.junit.Assert.assertEquals;

/**
 * Created by Remco Janssen on 18-1-2016.
 */
public class ArbiterTest {

    /** Some pLayers to create teams and mathces */
    private Player player1;

    private Player player2;

    private Player player3;

    private Player player4;

    private Arbiter mArbiter;

    @Before
    public void setUp() throws Exception {
        /** Create a few players */
        player1 = new HumanPlayer("1", "Remco", "15", "");
        player2 = new HumanPlayer("2", "Eva", "none", "");
        player3 = new HumanPlayer("3", "Bons", "20", "");
        player4 = new HumanPlayer("4", "vanRiel", "16", "");
        List<Team> teams = generateTeams();
        mArbiter = new Arbiter.Builder(new BaseRules.Builder().withTeams(teams).build()).build();
    }

    @Test
    public void testScoreCannotBeSubmittedByNonPlayingPlayer() throws ArbiterException {
        assertEquals(player1, mArbiter.getCurrentPlayer());
        mArbiter.validateScore(player2, new Turn(0, 0, 0, 0, 0, 0));
        assertEquals(player1, mArbiter.getCurrentPlayer());
    }

    @Test
    public void testUndoingScore() throws ArbiterException {
        Turn turn1 = new Turn(0, 0, 0, 0, 0, 0);
        Turn turn2 = new Turn(20, 3, 19, 3, 15, 2);
        Turn turn3 = new Turn(20, 1, 20, 1, 20, 1);
        assertEquals(player1, mArbiter.getCurrentPlayer());
        mArbiter.validateScore(player1, turn1);
        assertEquals(player3, mArbiter.getCurrentPlayer());
        assertEquals(turn1.getTotal(), mArbiter.undoScore().getTotal());

        assertEquals(player1, mArbiter.getCurrentPlayer());
        mArbiter.validateScore(player1, turn2);
        assertEquals(player3, mArbiter.getCurrentPlayer());
        mArbiter.validateScore(player3, turn3);
        assertEquals(player2, mArbiter.getCurrentPlayer());
        assertEquals(turn3.getTotal(), mArbiter.undoScore().getTotal());
        assertEquals(player3, mArbiter.getCurrentPlayer());
        assertEquals(turn2.getTotal(), mArbiter.undoScore().getTotal());
        assertEquals(player1, mArbiter.getCurrentPlayer());
        assertEquals(null, mArbiter.undoScore());
        assertEquals(null, mArbiter.undoScore());
    }

    private List<Team> generateTeams() throws UnknownPlayerException {
        List<Team> teams = new ArrayList<>(2);
        teams.add(new HumanTeam("uuid1", Arrays.asList(player1, player2)));
        teams.add(new HumanTeam("uuid2", Arrays.asList(player3, player4)));
        return teams;
    }
}
