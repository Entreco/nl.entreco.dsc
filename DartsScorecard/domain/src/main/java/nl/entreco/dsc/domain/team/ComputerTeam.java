package nl.entreco.dsc.domain.team;

import java.util.List;

import nl.entreco.dsc.domain.player.Player;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
public class ComputerTeam extends BaseTeam {


    public ComputerTeam(final String _uuid, final List<Player> _players) {
        super(_uuid, _players);
    }

    @Override
    public boolean isHuman() {
        return false;
    }
}
