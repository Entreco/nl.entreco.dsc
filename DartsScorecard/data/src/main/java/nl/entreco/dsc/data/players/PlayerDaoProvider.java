package nl.entreco.dsc.data.players;

/**
 * Created by Remco Janssen on 16-1-2016.
 */

import com.activeandroid.query.Select;

import android.support.annotation.NonNull;

import nl.entreco.dsc.domain.PlayerProvider;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;

/**
 * Created by Remco Janssen on 16-1-2016.
 */

/**
 * Created by Remco Janssen on 13-1-2016.
 */
public class PlayerDaoProvider implements PlayerProvider {

    private static int sNumberOfComputerPlayers = 0;

    /**
     * Creates a new player, which is stored in the database with a unique UUID
     *
     * @param _name      of the player
     * @param _favDouble of the player [bull,20,...,1,none]
     * @param _profile   of the profile picture
     */
    @NonNull
    @Override
    public Player createHumanPlayer(@NonNull final String _name, @NonNull final String _favDouble,
            @NonNull final String _profile) {
        PlayerDao player = new PlayerDao();
        player.setName(_name);
        player.setFavDouble(_favDouble);
        player.setProfile(_profile);
        player.save();
        return PlayerMapper.daoToDomain(player, true);
    }

    @Override
    @NonNull
    public Player retrieveHumanPlayer(@NonNull final String _uuid) throws UnknownPlayerException {
        PlayerDao player = new Select().from(PlayerDao.class)
                .where(PlayerDao.Columns.UUID + "=?", _uuid)
                .executeSingle();
        if (player == null) {
            throw new UnknownPlayerException("No player with uuid:%s", _uuid);
        }
        return PlayerMapper.daoToDomain(player, true);
    }

    @Override
    @NonNull
    public Player createComputerPlayer() {
        PlayerDao player = new PlayerDao();
        player.setName(String.format("Computer %d", ++sNumberOfComputerPlayers));
        player.setFavDouble("16");
        player.setProfile("");
        player.save();
        return PlayerMapper.daoToDomain(player, false);
    }
}
