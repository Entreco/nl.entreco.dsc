package nl.entreco.dsc.data.team;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.UUID;

import nl.entreco.dsc.data.TeamPlayerDao;
import nl.entreco.dsc.data.players.PlayerDao;

/**
 * Created by Remco Janssen on 16-1-2016.
 */
@Table(name = "Teams")
public class TeamDao extends Model {

    public static class Columns {

        public static final String UUID = "uuid";
    }

    @Column(name = Columns.UUID)
    private final String mUuid;


    public TeamDao() {
        mUuid = UUID.randomUUID().toString();
    }

    @NonNull
    public String getUuid() {
        return mUuid;
    }

    public List<PlayerDao> getPlayers() {
        return new Select()
                .from(PlayerDao.class)
                .innerJoin(TeamPlayerDao.class).on(
                        "Players.uuid" + " = " + "TeamPlayers.player")
                .where(TeamPlayerDao.Columns.TEAM + "=?", mUuid)
                .execute();
    }
}
