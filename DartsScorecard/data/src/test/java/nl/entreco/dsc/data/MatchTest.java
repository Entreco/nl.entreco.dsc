package nl.entreco.dsc.data;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.dsc.data.match.MatchDaoProvider;
import nl.entreco.dsc.data.players.PlayerDaoProvider;
import nl.entreco.dsc.data.team.TeamDaoProvider;
import nl.entreco.dsc.domain.MatchManager;
import nl.entreco.dsc.domain.PlayerManager;
import nl.entreco.dsc.domain.TeamManager;
import nl.entreco.dsc.domain.entities.BaseRules;
import nl.entreco.dsc.domain.entities.Turn;
import nl.entreco.dsc.domain.match.Match;
import nl.entreco.dsc.domain.player.Player;
import nl.entreco.dsc.domain.player.UnknownPlayerException;
import nl.entreco.dsc.domain.team.Team;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Remco Janssen on 17-1-2016.
 */
public class MatchTest extends DscApplicationTestcase {

    private MatchManager mMatchManager;

    private TeamManager mTeamManager;

    private PlayerManager mPlayerManager;


    /** Some pLayers to create teams and mathces */
    private Player player1;

    private Player player2;

    private Player player3;

    private Player player4;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mMatchManager = new MatchManager(new MatchDaoProvider());
        mTeamManager = new TeamManager(new TeamDaoProvider());
        mPlayerManager = new PlayerManager(new PlayerDaoProvider());

        mPlayerManager.createHumanPlayer("Remco", "15", "");

        /** Create a few players */
        player1 = mPlayerManager.createHumanPlayer("Remco", "15", "");
        player2 = mPlayerManager.createHumanPlayer("Eva", "none", "");
        player3 = mPlayerManager.createHumanPlayer("Bons", "20", "");
        player4 = mPlayerManager.createHumanPlayer("vanRiel", "16", "");

    }

    @Test
    public void testCreateMatch() throws UnknownPlayerException {
        List<Team> teams = generateTeams();
        Match match = mMatchManager.createMatch01(teams);
        assertNotNull(match);
        assertEquals(2, match.getNumberOfTeams());
        assertEquals(4, match.getNumberOfPlayers());

        // Check al Individual Players
        assertEquals(player1, match.getTeam(0).getPlayer(0));
        assertEquals(player2, match.getTeam(0).getPlayer(1));
        assertEquals(player3, match.getTeam(1).getPlayer(0));
        assertEquals(player4, match.getTeam(1).getPlayer(1));
    }

    @Test
    public void testRetrieveMatch() throws UnknownPlayerException {
        Match createdMatch = mMatchManager.createMatch01(generateTeams());
        Match retrievedMatch = mMatchManager.retrieveMatch(createdMatch.getUuid());
        assertEquals(createdMatch, retrievedMatch);
    }

    @Test
    public void testPlayMatch() throws UnknownPlayerException {
        List<Team> teams = generateTeams();
        Match match = mMatchManager.createMatch01(teams);
        match.onStartWithRules(new BaseRules.Builder().isTraining(true).withTeams(teams).build());
        Player nextPlayer = match.doSubmitScore(player1, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 19, 3, 12, 2));// leg 1
        assertEquals("Bons", nextPlayer.getName());// Bons starts 2nd leg
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        assertEquals("Eva", nextPlayer.getName());
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 19, 3, 12, 1));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 19, 3, 12, 2));// leg 2
        assertEquals("Eva", nextPlayer.getName());// Eva starts 2nd leg
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        assertEquals("vanRiel", nextPlayer.getName());
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 1, 2));
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 19, 3, 12, 2));// leg 3
        assertEquals("vanRiel", nextPlayer.getName());// vanRiel starts 2nd leg
        nextPlayer = match.doSubmitScore(nextPlayer, new Turn(20, 3, 20, 3, 20, 3));
        assertEquals("Remco", nextPlayer.getName());

    }

    private List<Team> generateTeams() throws UnknownPlayerException {
        List<Team> teams = new ArrayList<>(2);
        teams.add(mTeamManager.create(player1, player2));
        teams.add(mTeamManager.create(player3, player4));
        return teams;
    }
}
