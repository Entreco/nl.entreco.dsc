package nl.entreco.dsc.domain.entities;

/**
 * Simple wrapper to submit a single Turn.
 * Created by Remco Janssen on 18-1-2016.
 */
public class Turn implements DartThrowable{

    final int mDart1;

    final int mDart2;

    final int mDart3;

    final int mMultiplier1;

    final int mMultiplier2;

    final int mMultiplier3;

    private final int mTotal;

    public Turn(int _d1, int _m1, int _d2, int _m2, int _d3, int _m3) {
        mDart1 = _d1;
        mDart2 = _d2;
        mDart3 = _d3;
        mMultiplier1 = _m1;
        mMultiplier2 = _m2;
        mMultiplier3 = _m3;
        mTotal = mDart1 * mMultiplier1 + mDart2 * mMultiplier2 + mDart3 * mMultiplier3;
    }

    @Override
    public int getTotal() {
        return mTotal;
    }
}
